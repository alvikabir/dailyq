// sleep.c 
#include <time.h> 
#include"k.h" 
 
K nsleep(K nanos){ 
    struct timespec t; 
    t.tv_sec  = nanos->j / 1000000000ULL; 
    t.tv_nsec = nanos->j % 1000000000ULL; 
    return ki(nanosleep(&t, &t)); 
} 
 
// generate shared object 
// gcc -shared -fPIC -DKXVER=3 sleep.c -o sleep.so
