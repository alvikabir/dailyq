sfsmpl:{[d;c;i;s] 
    l:$[98h=type d;d c;d]; // normalize data to list 
    g:group $[11h=type l;(::);i xbar] l; // bucket data indices based on interval or symbol values 
    cnts:"j"$s*value %[;count d] count each g; // calculate sample sizes by multipling bucket ratios with sample size  
    raze d (neg cnts) ?' value g // select indices proportional to population distribution and apply to data 
 }
