// gcc -shared -fPIC -DKXVER=3 xor.c -o xor.so -Wall
#include "k.h"

K xor(K n1, K n2) {
    int t1 = n1->t; 
    int t2 = n2->t;
    if(!((t1 == -KH) || (t1 == -KI) || (t1 == -KJ))) return krr("type");
    if(!((t2 == -KH) || (t2 == -KI) || (t2 == -KJ))) return krr("type");
    K result;
    int v1, v2;
    switch(t1) {
        case -KH:
            v1 = (int)n1->h;
            break;
        case -KI:
            v1 = (int)n1->i;
            break;
        case -KJ:
            v1 = (int)n1->j;
            break; 
    }
    switch(t2) {
        case -KH:
            v2 = (int)n2->h;
            result = kh(v1^v2);
            break;
        case -KI:
            v2 = (int)n2->i;
            result = ki(v1^v2);
            break;
        case -KJ:
            v2 = (int)n2->j;
            result = kj(v1^v2);
            break; 
    }
     
    return result;
}
