//gcc -shared -fPIC -DKXVER=3 shift.c -o shift.so -Wall
#include "k.h"
#include <stddef.h>

K lshift(K num, K n) {
    int t = num->t; 
    if(!((t == KJ) || (t == -KJ))) return krr("type");
    if(!(n->t == -KJ)) return krr("type");
    K result;
    if(t == KJ) {
        result = ktn(KJ, num->n);
        for(size_t i=0;i<num->n;++i){
            kJ(result)[i] = kJ(num)[i]<<n->j;
        }
    }
    else {
        result = kj((num->j)<<n->j);
    }
    return result;
}

K rshift(K num, K n) {
    int t = num->t; 
    if(!((t == KJ) || (t == -KJ))) return krr("type");
    if(!(n->t == -KJ)) return krr("type");
    K result;
    if(t == KJ) {
        result = ktn(KJ, num->n);
        for(size_t i=0;i<num->n;++i){
            kJ(result)[i] = kJ(num)[i]<<n->j;
        }
    }
    else {
        result = kj((num->j)>>n->j);
    }
    return result;
}
