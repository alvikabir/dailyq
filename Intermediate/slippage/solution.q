lag:00:00:01;
order:([]time:10:00 10:00:00.001n;sym:`AAPL;side:`buy`sell;quantity:300 600);
quote:([]time:10:00 10:00:00.1n;sym:2#`AAPL;bid_price:351.73 351.65;ask_price:352.43 352.01);
data:(aj[`sym`time;order;select time,sym,expected_bid:bid_price,expected_ask:ask_price from quote]) lj `time xkey aj[`sym`exchTime;`exchTime`sym xcols update exchTime:time+lag from order;`exchTime`sym xcols select exchTime:time,sym,actual_bid:bid_price,actual_ask:ask_price from quote];
data:(cols[order],`slippage_bps`slippage_abs)# update slippage_bps:10000*?[side=`buy;neg %[;expected_ask] actual_ask-expected_ask;%[;expected_bid] actual_bid-expected_bid], slippage_abs:quantity*?[side=`buy;neg actual_ask-expected_ask;actual_bid-expected_bid] from data;
