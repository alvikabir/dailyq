collapseDict:{(value x) group key x}
pvt:{[t;k;p;v;f] 
    dp:distinct `$string asc t p;
    0!?[t;();((),k)!(),k;(#;enlist dp;(each;f;(collapseDict;(!;(`$;(string;p));v))))]
 };
