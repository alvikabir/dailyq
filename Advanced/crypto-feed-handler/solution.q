trade:([]time:();product_id:();sequence:();price:();open_24h:();volume_24h:();low_24h:();high_24h:();volume_30d:();best_bid:();best_ask:();side:();trade_id:();last_size:());
.z.wc:{-1"Disconnected, hdl:",string x;};
.z.ws:{
    data:update `$product_id, "F"$price, "F"$open_24h, "F"$volume_24h, "F"$low_24h, "F"$high_24h, "F"$volume_30d, "F"$best_bid, "F"$best_ask, `$side, "P"$-1_time, "F"$last_size from .j.k x;
    .[insert;(`trade;value cols[trade]#data);()];
 };
con:{@[;0](`$":wss://ws-feed.pro.coinbase.com")"GET / HTTP/1.1\r\nHost: ws-feed.pro.coinbase.com\r\n\r\n"};
sub:{[h;products]
    neg[h] .j.j `type`channels!(`subscribe;enlist (`name`product_ids!(`ticker;(),products)));
 };
unsub:{[h;products]
    neg[h] .j.j `type`product_ids`channels!(`unsubscribe;(),products;(),`ticker);
 };

// connect and subscribe to products
h:con[];
products:`$("BTC";"ETH";"XRP";"LTC";"BCH";"OXT";"XLM";"ATOM";"ETC";"LINK";"ALGO";"DAI"),\:"-USD";
sub[h;products];
