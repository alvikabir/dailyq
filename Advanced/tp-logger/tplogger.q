\d .tpl
opts:.Q.def[`tp`outpath!(enlist":5010";`:.)] .Q.opt .z.x;
tpHostPort:hsym `$raze ":",opts`tp;
tpLogDir:hsym opts`outpath;

connectTP:{
  host:@[;1] hp:`$":" vs string tpHostPort;
  if[host in ``localhost,.z.h;tpHostPort::` sv `:unix:/,hp 2];
  tpH::@[hopen;(tpHostPort;1000);0Ni];
 };

openLog:{[dir;date]
  tpLogFile::` sv dir, ` sv `tp,`$string date;
  tpLogFile set ();
  tpLogH::hopen tpLogFile;
 }[tpLogDir];

init:{
  connectTP[];
  tpDate:last .tpl.tpH"(.u.sub[`;`];.u.d)";
  openLog[tpDate];
 };

\d .

upd:{[tbl;data]
  .tpl.tpLogH enlist (`upd;tbl;data);
  .tpl.logCount+:1;
 };

.u.end:{
  hclose .tpl.tpLogH;
  .tpl.openLog x+1;
  .tpl.logCount:0;
 };

.z.ts:{
  if[null .tpl.tpH;
    -1"Retrying connection to tickerplant";
    .tpl.connectTP[];
  ];
 };

.z.pc:{
  system"sleep 5s";
  -1"Retrying connection to tickerplant";
  .tpl.connectTP[];
 };

.tpl.init[];
