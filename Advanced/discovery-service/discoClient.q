\d .discoClient

discoHdl:0Ni;

args:.Q.def[`name`serviceType`region`discoHost`discoPort`sendHB!(`;`;`;`;0Ni;0b)] .Q.opt .z.x;

connect:{ 
    conn:`;
    if[null args[`discoPort]; '"No discovery port specified"];
    $[(args[`discoHost]=.z.h) or (null args[`discoHost]) or args[`discoHost]=`localhost;
         conn:.Q.dd[`:unix:/;args[`discoPort]];
         conn:hsym `$":" sv string (args[`discoHost];args[`discoPort]);
    ];
    h:@[hopen;(conn;1000);{discoHdl::0Ni;'x}];
    discoHdl::h;
 };

disconnect:{ 
    @[hclose;discoHdl;()];
    discoHdl::0Ni;
 };

sendUpd:{
    d:`name`pid`serviceType`region`hostname`port`qVersion`qVersionDate!(args`name;.z.i;args`serviceType;args`region;.z.h;system"p";.z.K;.z.k);
    neg[discoHdl](`upd;d);
 };

// get host and port for service(s) requested, pass in dictionary, ex. `serviceType`region!`rdb`amrs
getServiceAll:{ discoHdl(`getServiceAll;x)};
getService:{ discoHdl(`getService;x)};

.z.pc:{
    if[x=discoHdl;
        disconnect[];
    ];
 };

// if disconnected try to reconnect on next timer cycle
.z.ts:{
    if[null discoHdl;connect[]];
    if[args[`sendHB] and not null discoHdl; sendUpd[]];
 };

connect[];
