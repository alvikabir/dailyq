status:([]
    name:`$();
    time:`timestamp$();
    pid:`int$();
    serviceType:`$();
    region:`$();
    hostname:`$();
    port:`int$();
    qVersion:`float$();
    qVersionDate:`date$();
    ipcHandle:`int$();
    active:`boolean$()
 );

latestStatus:1!status;

upd:{ 
    time:.z.p;
    // new ipc handle for calling service, close old one
    if[not[null oldH] and not .z.w~oldH:latestStatus[x`name]`ipcHandle;
        `status upsert @[;`active;:;0b] @[;`time;:;time] exec from status where ipcHandle=oldH;
        @[hclose;oldH;()];
    ];
    row:(x`name;time;x`pid;x`serviceType;x`region;x`hostname;x`port;x`qVersion;x`qVersionDate;.z.w;1b);
    `status upsert row;
    `latestStatus upsert row;
 };

// check disconnected handle against latestStatus and update tables if necessary
.z.pc:{
    if[not[null inactive] and count inactive:first exec name from latestStatus where ipcHandle=x;
        update ipcHandle:0Ni, active:0b from `latestStatus where name=inactive;
        `status upsert @[;`time;:;.z.p] exec from latestStatus where name=inactive;
    ];
 };

// only accept function calls from sync messages
ALLOWED_FNS:`getService`getServiceAll;
.z.pg:{
    if[10h=type x;x:parse x];
    if[not -11h=type fn:x 0;'"Not allowed"];
    if[not fn in ALLOWED_FNS;'"Not allowed"];
    value x
 };

// only accept heartbeats from async messages
.z.ps:{
    if[10h=type x;x:parse x];
    if[not `upd~x 0;'"Not allowed"];
    value x;
 };

createHostPort:{[h;p]
    if[not[count h] or not count p;:`];
    if[all[null h] or all null p;:`];
    hp:hsym `$(":" sv) each string ((),h),'(),p;
    $[1=count hp;first hp;hp]
 };

// discovery functions
getServiceAll:{[opts]
    allowedFilters:`name`serviceType`region`hostname`port`qVersion`qVersionDate;
    opts:(key[opts] inter allowedFilters)#opts;
    if[not count opts;:`$()];
    // select from latestStatus where active, not ipcHandle=.z.w, ...
    filters:(`active;(not;(=;`ipcHandle;`.z.w))),key[opts] {fn:(=;like) x=`name;(fn;x;$[-11h=type y;1#;(::)] y)}' value[opts];
    svcs:?[`latestStatus;filters;0b;()];
    except[;`] (),exec createHostPort[hostname;port] from svcs
 };
getService:{[opts] first -1?getServiceAll[opts] };
