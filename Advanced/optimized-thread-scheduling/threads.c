// threads.c 
#include <time.h> 
#include <pthread.h> 
#include <stdlib.h> 
#include"k.h" 
 
pthread_mutex_t lock; 
int currThreads; 
 
struct arguments { 
    int threadNo; 
    int itemNo; 
    K e; 
    int *threadState; 
    int *finished; 
    K results; 
}; 
 
K nsleep(K nanos){ 
    int type = nanos->t; 
    if((type != -KJ) && (type != -KN)) return krr("type"); 
    struct timespec t; 
    t.tv_sec  = nanos->j / 1000000000ULL; 
    t.tv_nsec = nanos->j % 1000000000ULL; 
    return ki(nanosleep(&t, &t)); 
} 
 
void* run(void *args) { 
    struct arguments *a = (struct arguments*)args; 
    kI(a->results)[a->itemNo] = nsleep(a->e)->j; 
    pthread_mutex_lock(&lock); 
    a->threadState[a->threadNo] = 0; 
    --currThreads; 
    pthread_mutex_unlock(&lock); 
    a->finished[a->itemNo] = 1; 
    free(a); 
    return 0; 
} 
 
// parallel nanosleep 
K psleep(K list, K n) { 
    int type = list->t; 
    if(type < 0) return nsleep(list); // running psleep on atom 
    if((type != KJ) && (type != KN)) return krr("type"); 
    int numItems = list->j; 
    int maxThreads = n->j; 
    if(maxThreads > numItems) maxThreads = numItems; 
    int finished[numItems], processed[numItems], threadState[maxThreads]; 
    K results = ktn(KI, numItems); 
    for(int i = 0; i  < numItems; ++i){ 
        finished[i] = 0; 
        processed[i] = 0; 
    } 
    for(int i = 0; i < maxThreads; ++i) threadState[i] = 0; 
    pthread_t tid[maxThreads]; 
    while(1) { 
        int done = 1; 
        for(int i = 0; i  < numItems; ++i) { 
            if(finished[i] == 0) { 
                    done = 0; 
                    break; 
            } 
        } 
        if(done) break; 
        if(currThreads >= maxThreads) continue; 
        int threadNo = -1; 
        for (int i = 0; i < maxThreads; ++i) { 
            if(threadState[i] == 0) { 
                    threadNo = i; 
                    break; 
            } 
        } 
        if(threadNo != -1) { 
            int itemNo = -1; 
            for(int i = 0; i < numItems; ++i) { 
                if(processed[i] == 0) { 
                    itemNo = i; 
                    break; 
                } 
            } 
            if(itemNo != -1) { 
                struct arguments *args = malloc(sizeof(struct arguments)); 
                args->threadNo = threadNo; 
                args->itemNo = itemNo; 
                args->e = kj(kJ(list)[itemNo]); 
                args->threadState = threadState; 
                args->finished = finished; 
                args->results = results; 
                threadState[threadNo] = 1; 
                processed[itemNo] = 1; 
                ++currThreads; 
                pthread_create(&tid[threadNo], NULL, run, (void *)args); 
            } 
        } 
    } 
    return results; 
} 
 
// generate shared object  
// gcc -shared -fPIC -DKXVER=3 threads.c -o threads.so -Wall
